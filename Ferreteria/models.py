from django.db import models

# Create your models here.

class Categoria(models.Model):                                              #Profesion
   nombre = models.CharField(max_length=200)



class Tipo(models.Model):                                                  # Habilidades
   nombre = models.CharField(max_length=200)
   #  related_name sirve para poder definir el como se va a llamar desde se clase relacionada,
   #  por ejemplo en este caso con categoria.tipos.all() se va a poder obtener todos los tipos
   categoria = models.ForeignKey(Categoria, on_delete=models.CASCADE, related_name='tipos')

class Marca(models.Model):                     #otra
   nombre = models.CharField(max_length=200)
   #  related_name sirve para poder definir el como se va a llamar desde se clase relacionada,
   #  por ejemplo en este caso con tipo.marcas.all() se va a poder obtener todas las marcas
   tipo = models.ForeignKey(Tipo, on_delete=models.CASCADE, related_name='marcas')


class Producto(models.Model):
   nombre = models.CharField(max_length=200)
   #fecha_cracion=models.DateTimeField(auto_now_add=True, null=True)
   #precio = models.FloatField(null=True)
   #cantidad = models.IntegerField()
   #  related_name sirve para poder definir el como se va a llamar desde se clase relacionada,
   #  por ejemplo en este caso con categorias.productos.all() se va a poder obtener todos los productos
   categoria = models.ForeignKey(Categoria, on_delete=models.CASCADE, related_name='productos')

