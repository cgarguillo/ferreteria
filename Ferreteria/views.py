from django.shortcuts import render

from.models import Categoria, Tipo, Marca, Producto

# Create your views here.

def cargar_categoria(request):

   if request.method == 'POST':
       nombre = request.POST.get('nombre')            #viene de la info que cargamos en profesion_carga.html

       #if int(nombre) == nombre:
           #error = "Error categoria no puede contener valores duplicados"
           #return render(request, 'categoria_carga.html',{'lista_categoria':lista_categoria, 'error': error})


       categoria = Categoria()
       categoria.nombre = nombre
       categoria.save()

   lista_categoria = Categoria.objects.all()
   return render(request, 'categoria_carga.html',{'lista_categoria':lista_categoria})

def cargar_tipo(request):
   if request.method == 'POST':
       nombre = request.POST.get('nombre')
       categoria_id = request.POST.get('categoria')
       tipo = Tipo()
       tipo.nombre = nombre
       tipo.categoria_id = categoria_id
       tipo.save()

   lista_tipo = Tipo.objects.all()
   lista_categoria = Categoria.objects.all()
   return render(request, 'tipo_carga.html',{'lista_tipo':lista_tipo, 'lista_categoria':lista_categoria})

def cargar_marca(request):

   lista_marca = Marca.objects.all()
   lista_tipo = Tipo.objects.all()

   if request.method == 'POST':
       nombre = request.POST.get('nombre')
       tipo_id = request.POST.get('tipo')
       marca = Marca()
       marca.nombre = nombre
       marca.tipo_id = tipo_id
       marca.save()

   return render(request, 'marca_carga.html', {'lista_marca': lista_marca, 'lista_tipo': lista_tipo})


def cargar_producto(request):
    lista_Producto = Producto.objects.all()
    lista_categoria = Categoria.objects.all()
    lista_marca = Marca.objects.all()
    lista_tipo = Tipo.objects.all()


    if request.method == 'POST':
        nombre = request.POST.get('nombre')
        categoria_id = request.POST.get('categoria')
        producto = Producto()
        producto.nombre = nombre
        producto.categoria_id = categoria_id
        producto.save()

    return render(request, 'producto_carga.html', {'lista_Producto': lista_Producto, 'lista_categoria': lista_categoria,
                                                 'lista_marca': lista_marca, 'lista_tipo': lista_tipo })